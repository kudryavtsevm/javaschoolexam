package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */

    /*
    The distribution of numbers in the desired pyramid is distributed similar to Floyd's triangle
    The pyramid can be built only if the number of elements in the original array is a triangular number
    */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        if(inputNumbers.contains(null)) throw new CannotBuildPyramidException();
        if (!isTriangularNumber(inputNumbers.size())) throw new CannotBuildPyramidException();
        int rows = getRowNumbers(inputNumbers);
        int columns = 2 * rows - 1;
        int [][] pyramid = new int[rows][columns];
        try {
            Collections.sort(inputNumbers);
        } catch (NullPointerException e) {
            throw new CannotBuildPyramidException();
        }

        Queue<Integer> queue = new LinkedList<>(inputNumbers);
        int startPosition = (pyramid[0].length) / 2;
        for(int i = 0; i < pyramid.length; i++)
        {
            int start = startPosition;
            for(int j = 0; j <= i; j++)
            {
                pyramid[i][start] = queue.remove();
                start += 2;
            }
            startPosition--;
        }

        return pyramid;
    }

    private boolean isTriangularNumber(int x)
    {
        if (x < 0)
            return false;

        int n = (int) Math.sqrt(2 * x);
        return n * (n + 1) / 2 == x;
    }
    /*
    We get the number of rows in the pyramid from the formula for calculating triangular numbers
    x = n*(n-1)/2
    Then the number of rows is equal to
    r = (sqrt(8*x+1)-1)/2
    */
    private int getRowNumbers(List<Integer> inputNumbers) {
        int listSize = inputNumbers.size();
        return (int)(Math.sqrt(8 * listSize + 1) - 1)/2;
    }


}
