package com.tsystems.javaschool.tasks.calculator;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Stack;

public class Calculator {
    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */

    //To implement the stack calculator, use the Reverse Polish Notation (RPN)
    public String evaluate(String statement) {
        Stack<Double> stack = new Stack<>();
        if (!validateStatement(statement)) return null;
        String postfix = statementToRPN(statement);
        String [] operand = postfix.split(" ");
        for (int i = 0; i < operand.length; i++) {
            if (isDouble(operand[i])) {
                stack.push(Double.parseDouble(operand[i]));
            }
            else {
                double a = stack.pop();
                double b = stack.pop();
                switch (operand[i]) {
                    case "+":
                        stack.push(b + a);
                        break;
                    case "-":
                        stack.push(b - a);
                        break;
                    case "*":
                        stack.push(b * a);
                        break;
                    case "/":
                        if (a == 0) return null;
                        stack.push(b / a);
                        break;
                }
            }

        }
        DecimalFormatSymbols symbols = new DecimalFormatSymbols();
        symbols.setDecimalSeparator('.');
        DecimalFormat df = new DecimalFormat("0.####", symbols);
        return (df.format(stack.pop()));

    }

    //To translate the original expression into postfix notation (RPN), use the Dijkstra algorithm
    public String statementToRPN(String input) {
        Stack<Character> stack = new Stack<>();
        String output = "";
        char currentChar;
        int priority;
        for (int i = 0; i < input.length(); i++){
            currentChar = input.charAt(i);
            priority = getPriority(currentChar);
            if (currentChar == ' ') continue;
            if (Character.isDigit(currentChar) || currentChar == '.') output += currentChar;
            else if (priority == 0) stack.push(currentChar);
            else if (priority > 1) {
                output += ' ';
                while (!stack.empty()) {
                    if (getPriority(stack.peek()) >= priority) {
                        output += stack.pop();
                        output += ' ';
                    }
                    else break;
                }
                stack.push(currentChar);
            }
            else if (priority == 1) {
                output += ' ';
                while (getPriority(stack.peek()) != 0) {
                    output += stack.pop();
                    //output += ' ';
                }
                stack.pop();
            }
            else return null;
        }
        while (!stack.empty()) {
            output += ' ';
            output += stack.pop();
        }
        return output;
    }

    private int getPriority(char c){
        if (c == '(') return 0;
        else if (c == ')') return 1;
        else if (c == '+' || c == '-') return 2;
        else if (c == '*' || c == '/') return 3;
        else return -1;
    }

    public boolean validateStatement (String statement) {
        if (statement == null) return false;
        if (statement.isEmpty()) return false;
        statement = statement.replaceAll("\\s", "");

        int openParentCount = 0;
        int closeParentCount = 0;
        char currentChar;
        for (int i = 0; i < statement.length(); i++) {
            currentChar = statement.charAt(i);
            if (currentChar == '(') openParentCount++;
            else if (currentChar == ')') closeParentCount++;
            else if (!Character.isDigit(currentChar) &&
                    currentChar != '+' &&
                    currentChar != '-' &&
                    currentChar != '*' &&
                    currentChar != '/' &&
                    currentChar != '.')
                return false;
        }

        if ((statement.contains("..")) ||
                (statement.contains("++")) ||
                (statement.contains("--")) ||
                (statement.contains("**")) ||
                (statement.contains("//")))
            return false;

        if (openParentCount != closeParentCount) return false;
        return true;
    }

    private boolean isDouble (String s) {
        try {
            Double.parseDouble(s);
            return true;
        }
        catch (NumberFormatException e) {
            return false;
        }
    }

}
