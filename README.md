# README #

This is a repo with T-Systems Java School preliminary examination tasks.
Code points where you solution is to be located are marked with TODOs.

The exam includes 3 tasks to be done: [Calculator](/tasks/Calculator.md), [Pyramid](/tasks/Pyramid.md), and 
[Subsequence](/tasks/Subsequence.md)

### Result ###

* Author name : KUDRYAVTSEV MAXIM
* Codeship : [![Codeship Status for kudryavtsevm/javaschoolexam](https://app.codeship.com/projects/0dd9572a-69f1-42fc-b842-e5c88a467a88/status?branch=master)](https://app.codeship.com/projects/450409)


